# My Dotfiles
These are my dotfiles for River on GNU/Linux (Currently Artix Linux).

## Included:
- River dotfiles
- Minimal system configuration
- Nice wallpaper I made from a space image
- **DOES NOT INCLUDE THEMES/PACKAGES**

## Dependencies
- Catppuccin (gtk mocha and cursors mocha)
- Tela icon theme (purple dark)
- Symbols Nerd Font

You will need [yay](https://aur.archlinux.org/packages/yay) or any other AUR helper for installing packages. ***Please read the [package list](./packages.txt) before putting hundreds of my personal choices into your machine!***

To install the packages from my package list, you can run:
```bash
# will install all packages in the txt file
yay -S $(cat packages.txt) --needed
```

## Installation

#### Dotfiles
They can be put anywhere you'd like, you'll just have to use [GNU Stow](https://www.gnu.org/software/stow/).

```bash
mv <git-repo> <location>
cd <location>
stow -vt $HOME/.config .config
stow -vt $HOME/.local .local
```

#### Bash profile
To set the .bashrc file you can run inside the git repo
```bash
# will fail if you have a default .bashrc in $HOME
ln -s .config/bash-custom/.bashrc $HOME/.bashrc
```

#### Zsh profile (my current setup)
To set the .zshrc file you can run inside the git repo
**You will need autosuggestions and syntax highlighting installed.**
```bash
# will fail if you have a default .zshrc in $HOME
ln -s .config/bash-custom/.zshrc $HOME/.zshrc
```

You're all set! Talk to me if you have any issues or want help with my config or Linux in general.

<p align="center">
    <a href="https://mastodon.social/@augustotx"><img src="assets/social/macchiato_mastodon.svg" width="64" height="64" alt="Mastodon Logo"/></a>
    <a href="https://matrix.to/#/@augustotx:matrix.org"><img src="assets/social/macchiato_matrix.svg" width="64" height="64" alt="Matrix Logo"/></a>
</p>
