;; This is an operating system configuration generated
;; by the graphical installer.
;;
;; Once installation is complete, you can learn and modify
;; this file to tweak the system configuration, and pass it
;; to the 'guix system reconfigure' command to effect your
;; changes.


;; Indicate which modules to import to access the variables
;; used in this configuration.
(use-modules (gnu) (nongnu packages linux) (nongnu system linux-initrd))
(use-service-modules cups desktop networking ssh xorg)
(use-package-modules cups freedesktop vim wm video certs version-control
                     terminals xdisorg web-browsers zig-xyz package-management
		     shells disk linux commencement)

(operating-system
  (kernel linux)
  (initrd microcode-initrd)
  (firmware (list linux-firmware))
  (locale "en_US.utf8")
  (timezone "America/Sao_Paulo")
  (keyboard-layout (keyboard-layout "br"))
  (host-name "overguix")

  ;; The list of user accounts ('root' is implicit).
  (users (cons* (user-account
                  (name "rex")
                  (comment "Rex 2.0")
                  (group "users")
                  (home-directory "/home/rex")
                  (supplementary-groups '("wheel" "netdev" "audio" "video" "lpadmin")))
                %base-user-accounts))

  ;; Packages installed system-wide.  Users can also install packages
  ;; under their own account: use 'guix search KEYWORD' to search
  ;; for packages and 'guix install PACKAGE' to install a package.
  (packages (cons*  river swaybg swayidle swaylock neovim foot mpv
		    git wlr-randr waybar flatpak fish gparted
                    xdg-desktop-portal xdg-desktop-portal-wlr
                    xdg-desktop-portal-gtk v4l2loopback-linux-module
                    pipewire wireplumber gcc-toolchain %base-packages))

  ;; Below is the list of system services.  To search for available
  ;; services, run 'guix system search KEYWORD' in a terminal.
  (services (append (modify-services %desktop-services
		(guix-service-type config => (guix-configuration
		  (inherit config)
		    (substitute-urls
		      (append (list "https://substitutes.nonguix.org")
			%default-substitute-urls))
			  (authorized-keys
			    (append (list (plain-file "non-guix.pub"
			      "(public-key 
 				 (ecc 
  				   (curve Ed25519)
  				   (q #C1FD53E5D4CE971933EC50C9F307AE2171A2D3B52C804642A7A35F84F3A4EA98#)
  				 )
 			       )"))
				    %default-authorized-guix-keys)))))
                (list
                  (service cups-service-type
                    (cups-configuration
                      (web-interface? #t)
                      (extensions
                      (list cups-filters hplip-minimal)))))))
  (bootloader (bootloader-configuration
                (bootloader grub-efi-bootloader)
                (targets (list "/boot/efi"))
                (keyboard-layout keyboard-layout)))
  (swap-devices (list (swap-space
                        (target (uuid
                                 "b0dbd709-8fec-46f1-9fe1-b66d85128176")))))

  ;; The list of file systems that get "mounted".  The unique
  ;; file system identifiers there ("UUIDs") can be obtained
  ;; by running 'blkid' in a terminal.
  (file-systems (cons* (file-system
                         (mount-point "/")
                         (device (uuid
                                  "3060e513-61d1-4e9c-b1eb-8e764f6f4ec4"
                                  'ext4))
                         (type "ext4"))
                       (file-system
                         (mount-point "/boot/efi")
                         (device (uuid "6A91-2DC7"
                                       'fat32))
                         (type "vfat"))
                       (file-system
                         (mount-point "/ssd")
                         (device (uuid "df7619a1-2ac2-4ce0-a1da-d704662510c1"
                                       'btrfs))
                         (type "btrfs"))
                       (file-system
                         (mount-point "/hdd")
                         (device (uuid "d7b4ba0e-dcda-444c-b2a8-67ae08dbbdf8"
                                       'btrfs))
                         (type "btrfs"))
                       %base-file-systems)))
