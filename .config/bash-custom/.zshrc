# History settings
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000

# Enable useful Zsh options
setopt autocd extendedglob
# bindkey -v  # Enable vi-mode keybindings

# Zsh plugins: autosuggestions & syntax highlighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

# Path management
export PATH=$PATH:/home/rex/.cargo/bin:/home/rex/.local/bin
export $(dbus-launch)

# SSH session handling (similar to bashrc)
if [[ $- != *i* ]]; then
  [[ -n "$SSH_CLIENT" ]] && source /etc/profile
  return
fi

# Source the system-wide Zsh config if available
[ -f /etc/zshrc ] && source /etc/zshrc

# Aliases (same as your bashrc)
alias grep='grep --color=auto'
alias ip='ip -color=auto'
alias cd..='cd ..'
alias e='exit'
alias l='eza -lahF --color=always --icons --sort=size --group-directories-first'
alias ls='eza -lhF --color=always --icons --sort=size --group-directories-first'
alias lst='eza -lahFT --color=always --icons --sort=size --group-directories-first'
alias mv='mv -i'
alias wget='wget -c'
alias updatesys='yay && flatpak update'

# Git branch status function
git_branch_status() {
  local branch=""
  branch=$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')
  local git_status=$(git status --porcelain 2>/dev/null)
  local color=green
  if echo "$git_status" | grep -q "^ M"; then
    color=yellow
    branch="${branch}*"
  fi
  if echo "$git_status" | grep -qE "^ A|^\?\?"; then
    color=yellow
    branch="${branch}+"
  fi
  if echo "$git_status" | grep -q "^ D"; then
    color=yellow
    branch="${branch}-"
  fi

  if [[ -n "$branch" ]]; then
    branch="(%F{${color}}${branch}%F{reset})"
  fi
  echo "$branch"
}

# Zsh prompt, similar to bashrc
update_prompt() {
  PS1='%F{green}%n%f@%F{cyan}%m%f %F{blue}%~%F{reset} '$(git_branch_status)$'\n''⟩ '
}

precmd_functions+=(update_prompt)
update_prompt

# Auto-completion setup
zstyle :compinstall filename '/home/rex/.zshrc'
autoload -Uz compinit && compinit

# CTRL + ARROWS
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word
