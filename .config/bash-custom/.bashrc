# Export 'SHELL' to child processes.  Programs such as 'screen'
# honor it and otherwise use /bin/sh.
export SHELL

PATH=$PATH:/home/rex/.cargo/bin:/home/rex/.local/bin
export $(dbus-launch)

if [[ $- != *i* ]]
then
    # We are being invoked from a non-interactive shell.  If this
    # is an SSH session (as in "ssh host command"), source
    # /etc/profile so we get PATH and other essential variables.
    [[ -n "$SSH_CLIENT" ]] && source /etc/profile

    # Don't do anything else.
    return
fi

# Source the system-wide file.
[ -f /etc/bashrc ] && source /etc/bashrc

# Aliases
alias grep='grep --color=auto'
alias ip='ip -color=auto'
alias cd..='cd ..'
alias e='exit'
alias grep='grep --color=auto'
alias l='eza -lahF --color=always --icons --sort=size --group-directories-first'
alias ls='eza -lhF --color=always --icons --sort=size --group-directories-first'
alias lst='eza -lahFT --color=always --icons --sort=size --group-directories-first'
alias mv='mv -i'
alias wget="wget -c"
alias updatesys="yay && flatpak update"

# Prompt stuff
git_branch_status() {
    local branch
    local status
    branch=$(git symbolic-ref --short HEAD 2>/dev/null)

    if [ -z "$branch" ]; then
        # No git repository detected
        echo ""
        return
    fi

    status=$(git status --porcelain 2>/dev/null)
    if [ -z "$status" ]; then
        # No changes
        echo -e " (\033[0;32m$branch\033[0m)"
    else
        # Changes detected
        echo -e " (\033[0;33m$branch+\033[0m)"
    fi
}

PS1='\033[0;32m\u\033[0m@\033[0;36m\h\033[0m \033[0;34m\W\033[0m$(git_branch_status)\n⟩ '
