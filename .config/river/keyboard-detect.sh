#!/bin/bash

if [ $(uname -n) == "overlord" ]; then
  exit 0
fi

while true; do
  if [ $(libinput list-devices | grep "xiudi" | wc -l) != "0" ]; then
    riverctl input keyboard-1-1-AT_Translated_Set_2_keyboard events disabled
  else
    riverctl input keyboard-1-1-AT_Translated_Set_2_keyboard events enabled
  fi
done
