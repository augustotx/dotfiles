#!/bin/bash

# detects if system is my desktop PC or not

isoverlord=false

if [ "$(uname -n)" = "overlord" ]; then
  isoverlord=true
fi

# stop processes already running for hard restart
pkill waybar
pkill dunst
pkill nm-applet
pkill pipewire
pkill wireplumber
pkill swaybg
pkill swww-daemon
pkill swww
pkill swayidle
pkill polkit-gnome-au
pkill fcitx5

# auto starting audio
pipewire &
pipewire-pulse &
wireplumber &

# my chinese character typing provider
fcitx5 -D &

# network indicator
nm-applet --indicator &

# authentication agent (GUI sudo)
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &

# script that detects my external keyboard if I'm on my laptop
$HOME/.config/river/keyboard-detect.sh &

# script for my monitors if on desktop PC
$HOME/.config/river/monitors.sh &

if [ "$isoverlord" = true ]; then
  # status bar
  waybar -c $HOME/.config/waybar/config.json &

  # creates manipulated mono audio sink for my single speaker

  # waits for pipewire to start if not started
  while ! ps aux | grep -w pipewire | grep -v grep > /dev/null; do
    sleep 1
  done

  # gets my HDMI out and makes a new mono sink from the stereo signal
  hdmiout="alsa_output.pci-0000_0d_00.1.hdmi-stereo-extra3"

  pactl load-module module-remap-sink \
    sink_name=hdmi_mono master=$hdmiout \
    channels=1 channel_map=mono \

  # this is for my headphones - runs easyeffects with some bass enhancements
  # in the background. it's actually pretty sweet
  flatpak run com.github.wwmm.easyeffects --gapplication-service &

else
  # if in my laptop, set idle lock time
  swayidle -w timeout 300 $HOME/.config/system_scripts/lock.sh &
  # status bar for laptop
  waybar -c $HOME/.config/waybar/config-laptop.json &
fi
