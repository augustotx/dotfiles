#!/bin/bash
# colors are rrggbb and defined in another script
source $HOME/.config/system_scripts/colors.sh

swaylock --color $base \
 --inside-color $surface1 \
  --inside-clear-color $yellow \
  --line-clear-color $yellow \
  --ring-clear-color $surface0 \
  --text-clear-color $base \
  --inside-caps-lock-color $peach \
  --inside-wrong-color $red \
  --line-wrong-color $surface0 \
  --ring-wrong-color $surface0 \
  --text-wrong-color $base \
  --key-hl-color $green \
  --layout-bg-color $surface0 \
  --layout-border-color '00000000' \
  --layout-text-color $text \
  --ring-color $surface0 \
  -r --separator-color $surface0 \
  --inside-ver-color $blue \
  --line-ver-color $surface0 \
  --text-ver-color $base \
  --ring-ver-color $surface0 \
  --indicator-radius 50 \
  --image $HOME/.config/river/backgrounds/ApolloSoyuzBlur.png \
  --font 'JetBrains Mono' \
  --font-size 15 \
  --indicator-idle-visible \
  --indicator-y-position 555 # this is so it aligns with the wallpaper
