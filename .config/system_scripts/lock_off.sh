#!/bin/bash


if [ $(uname -n) != "overlord" ]; then
    $HOME/.config/system_scripts/lock.sh
    exit 0
fi

wlr-randr --output HDMI-A-1 --off
wlr-randr --output DP-1 --off

libinput debug-events | while read -r line; do
    if echo "$line" | grep -E "KEY|POINTER_MOTION|BUTTON"; then
        wlr-randr --output HDMI-A-1 --on
        wlr-randr --output DP-1 --on

        $HOME/.config/system_scripts/lock.sh &
        ~/.config/river/init &

        break
    fi
done
