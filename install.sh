#!/bin/bash

yay -Syu $(cat ./packages.txt) --needed
stow -vt $HOME/.config .config
stow -vt $HOME/.local .local
cd ./packages/catppuccin-gtk-theme-mocha
makepkg -si
cd ../..
